let name = prompt("Ваше имя?", "Гость");
let age = prompt("Ваш возраст?", "Введите число полных лет");

if (age < 18) {
    alert('You are not allowed to visit this website. \n Вам не разрешено посещать этот сайт.');
}

if (age >= 18 && age <= 22) {
    let result = confirm('Are you sure you want to continue? \n Вы уверены что хотите продолжить?');

    if (result) {
        alert(`Welcome, ${name}. \n Добро пожаловать, ${name}.`);
    } else {
        alert('Goodbye! \n До свидания.');
    }
}

if (age > 22) {
    alert(`Welcome, ${name}. \n Добро пожаловать, ${name}.`);
}
